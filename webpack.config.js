const fs = require('fs');
const path = require('path');
const webpack = require('webpack');

const context = path.resolve(__dirname, './application');
const isProduction = process.env.NODE_ENV === 'production';

// Webpack configuration
// Context
exports.context = context;

// Devtool
exports.devtool = isProduction ? '' : '#eval-source-map';

// Entry point
exports.entry = isProduction ? path.resolve(context, 'index.js') : [
  'react-hot-loader/patch',
  path.resolve(context, 'index.js')
];

// Resolvers
exports.resolve = {};
exports.resolve.alias = {
  components: path.resolve(context, 'components'),
  reducers: path.resolve(context, 'reducers'),
  sagas: path.resolve(context, 'sagas'),
  services: path.resolve(context, 'services')
};
exports.resolve.modules = [
  'node_modules'
];

// Output
exports.output = {
  path: path.resolve(__dirname, './public'),
  publicPath: isProduction ? '/' : 'http://0.0.0.0:3808/',
  filename: 'application.js'
};

// Rules
exports.module = {
  rules: [{
    test: /\.jsx?$/,
    loader: 'babel-loader',
    include: context
  }, {
    test: /\.sss$/,
    loader: `style-loader!css-loader?module&localIdentName=[path]-[name]__[local]!postcss-loader?parser=sugarss`,
    include: context
  }]
};

// Plugins
exports.plugins = isProduction ? [
  new webpack.optimize.UglifyJsPlugin({
    output: {comments: false},
    compress: {warnings: false, screw_ie8: true},
    sourceMap: false
  }),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('production')
    }
  })
] : [
  // Development
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NamedModulesPlugin()
];

// Development server
exports.devServer = isProduction ? {} : {
  port: 3808,
  host: '0.0.0.0',
  noInfo: true,
  hot: true,
  inline: true,
  historyApiFallback: true,
  headers: {'Access-Control-Allow-Origin': '*'},
  stats: {colors: true}
};
