const Koa = require('koa');
const _ = require('koa-route');
const fs = require('fs');
const path = require('path');
const moment = require('moment');
const Datastore = require('nedb');
const busboy = require('async-busboy');

const app = new Koa();
const PORT = process.env.PORT || 3000;
const UPLOADS_DIR = path.resolve(__dirname, 'uploads');
const script = process.env.NODE_ENV === 'production' ?
  `<script>${fs.readFileSync(path.resolve(__dirname, './public/application.js'), 'utf8')}</script>` :
  `<script src="//0.0.0.0:3808/application.js"></script>`;

const removedDB = new Datastore({filename: path.resolve(__dirname, 'db/removed.db'), autoload: true});
const savedDB = new Datastore({filename: path.resolve(__dirname, 'db/saved.db'), autoload: true});

const createDoc = () => ({date: new Date()});

app.use(_.get('/', async ctx => {
  const removed = await new Promise((resolve, reject) => removedDB.count({}, (err, count) => err ? reject() : resolve(count)));
  const saved = await new Promise((resolve, reject) => savedDB.count({}, (err, count) => err ? reject() : resolve(count)));

  ctx.body = `
    <!DOCTYPE html>
    <html>
    <head>
      <meta charset="UTF-8"/>
    </head>
    <body>
      <div id="mount"></div>
      <script>
        window.SAVED_STARS = ${saved || 0};
        window.REMOVED_STARS = ${removed || 0};
      </script>
      ${script}
    </body>
    </html>
  `
}));

app.use(_.get('/recorderWorker.js', ctx => {
  ctx.body = `
var recLength = 0,
  recBuffersL = [],
  recBuffersR = [],
  sampleRate;


self.onmessage = function(e) {
  switch(e.data.command){
    case 'init':
      init(e.data.config);
      break;
    case 'record':
      record(e.data.buffer);
      break;
    case 'exportWAV':
      exportWAV(e.data.type);
      break;
    case 'getBuffer':
      getBuffer();
      break;
    case 'clear':
      clear();
      break;
  }
};

function init(config){
  sampleRate = config.sampleRate;
}

function record(inputBuffer){
  recBuffersL.push(inputBuffer[0]);
  recBuffersR.push(inputBuffer[1]);
  recLength += inputBuffer[0].length;
}

function exportWAV(type){
  var bufferL = mergeBuffers(recBuffersL, recLength);
  var bufferR = mergeBuffers(recBuffersR, recLength);
  var interleaved = interleave(bufferL, bufferR);
  var dataview = encodeWAV(interleaved);
  var audioBlob = new Blob([dataview], { type: type });

  self.postMessage(audioBlob);
}

function getBuffer() {
  var buffers = [];
  buffers.push( mergeBuffers(recBuffersL, recLength) );
  buffers.push( mergeBuffers(recBuffersR, recLength) );
  self.postMessage(buffers);
}

function clear(){
  recLength = 0;
  recBuffersL = [];
  recBuffersR = [];
}

function mergeBuffers(recBuffers, recLength){
  var result = new Float32Array(recLength);
  var offset = 0;
  for (var i = 0; i < recBuffers.length; i++){
    result.set(recBuffers[i], offset);
    offset += recBuffers[i].length;
  }
  return result;
}

function interleave(inputL, inputR){
  var length = inputL.length + inputR.length;
  var result = new Float32Array(length);

  var index = 0,
    inputIndex = 0;

  while (index < length){
    result[index++] = inputL[inputIndex];
    result[index++] = inputR[inputIndex];
    inputIndex++;
  }
  return result;
}

function floatTo16BitPCM(output, offset, input){
  for (var i = 0; i < input.length; i++, offset+=2){
    var s = Math.max(-1, Math.min(1, input[i]));
    output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true);
  }
}

function writeString(view, offset, string){
  for (var i = 0; i < string.length; i++){
    view.setUint8(offset + i, string.charCodeAt(i));
  }
}

function encodeWAV(samples){
  var buffer = new ArrayBuffer(44 + samples.length * 2);
  var view = new DataView(buffer);

  /* RIFF identifier */
  writeString(view, 0, 'RIFF');
  /* RIFF chunk length */
  view.setUint32(4, 36 + samples.length * 2, true);
  /* RIFF type */
  writeString(view, 8, 'WAVE');
  /* format chunk identifier */
  writeString(view, 12, 'fmt ');
  /* format chunk length */
  view.setUint32(16, 16, true);
  /* sample format (raw) */
  view.setUint16(20, 1, true);
  /* channel count */
  view.setUint16(22, 2, true);
  /* sample rate */
  view.setUint32(24, sampleRate, true);
  /* byte rate (sample rate * block align) */
  view.setUint32(28, sampleRate * 4, true);
  /* block align (channel count * bytes per sample) */
  view.setUint16(32, 4, true);
  /* bits per sample */
  view.setUint16(34, 16, true);
  /* data chunk identifier */
  writeString(view, 36, 'data');
  /* data chunk length */
  view.setUint32(40, samples.length * 2, true);

  floatTo16BitPCM(view, 44, samples);

  return view;
}
  `;
}));

app.use(_.post('/save', async ctx => {
  await new Promise((resolve, reject) => savedDB.insert(createDoc(), (err, doc) => err ? reject() : resolve(doc)));
  ctx.body = '';
}));

app.use(_.post('/remove', async ctx => {
  await new Promise((resolve, reject) => removedDB.insert(createDoc(), (err, doc) => err ? reject() : resolve(doc)));
  ctx.body = '';
}));

app.use(_.post('/file', async ctx => {
  const {files, fields} = await busboy(ctx.req);

  await Promise.all(files.map(file =>
    new Promise((resolve, reject) => {
      const s = fs.createWriteStream(path.resolve(UPLOADS_DIR, `${moment().format('lll')}-${Math.floor(Math.random() * 1000)}.wav`));

      file.pipe(s);

      file.on('end', resolve);

      s.on('error', reject);
    })
  ));

  ctx.body = '';
}));

app.listen(PORT, () => console.log(`Application is running on port: ${PORT}`));
