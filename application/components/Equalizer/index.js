import React, {PropTypes as T} from 'react';
import cx from 'classnames';
import styles from './index.sss';

const Equalizer = ({isShown}) => (
  <div className={cx(styles.root, {[styles.active]: isShown})}>
    <canvas id="equalizer" width={window.innerWidth} height={300}/>
  </div>
);

Equalizer.propTypes = {
  isShown: T.bool.isRequired
};

export default Equalizer;
