import React, {Component} from 'react';
import moment from 'moment';
import styles from './index.sss';

class Timer extends Component {
  state = {
    time: '00:00'
  };

  render() {
    return (
      <div className={styles.root}>
        <time className={styles.time}>{this.state.time}</time>
        <p className={styles.max}>max 5 min</p>
      </div>
    );
  }

  componentDidMount() {
    this.interval = setInterval(this.onTime, 1000);
  }

  componentWillUnmount() {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
    }
  }

  onTime = () => {

    this.setState({time: moment(this.state.time, 'mm:ss').add(1, 's').format('mm:ss')});
  }
}

export default Timer;
