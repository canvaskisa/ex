import React from 'react';
import styles from './index.sss';

const Button = props => <button type="button" className={styles.root} {...props}/>;

export default Button;
