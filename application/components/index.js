import React, {PropTypes as T, Component} from 'react';
import cx from 'classnames';
import {connect} from 'react-redux';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import * as $$recorder from 'reducers/recorder';
import * as $$stars from 'reducers/stars';
import * as $$ball from 'reducers/ball';
import Button from './Button';
import Particles from './Particles';
import Waveforms from './Waveforms';
import Equalizer from './Equalizer';
import SaveOrRemove from './SaveOrRemove';
import Timer from './Timer';
import styles from './index.sss';

export class Application extends Component {
  static propTypes = {
    blob: T.object,
    url: T.string,
    state: T.oneOf([
      'INITIAL',
      'RECORDING_STARTED',
      'PREVIEW',
      'SAVE_OR_DELETE',
      'PREFINISH'
    ]).isRequired,
    onStart: T.func.isRequired,
    onPreview: T.func.isRequired,
    onDissolve: T.func.isRequired,
    onSave: T.func.isRequired,
    onRemove: T.func.isRequired,

    removedStars: T.number.isRequired,
    savedStars: T.number.isRequired,
    ball: T.bool.isRequired
  };

  render() {
    const {state, url, blob, onStart, onPreview, onDissolve, onSave, onRemove} = this.props;

    return (
      <div className={styles.root}>
        <div className={styles.overlay}/>

        <Equalizer isShown={state === 'RECORDING_STARTED'} key="EQUALIZER"/>

        <CSSTransitionGroup
          component="div"
          transitionEnterTimeout={1000}
          transitionLeaveTimeout={500}
          transitionName={{
            enter: styles.enter,
            enterActive: styles.enterActive,
            leave: styles.leave,
            leaveActive: styles.leaveActive
          }}
          >
          {state === 'INITIAL' ? (
            <Button key="START_BUTTON" onMouseMove={onStart}>Начать</Button>
          ) : null}

          {state === 'RECORDING_STARTED' ? [
            <Timer key="TIMER"/>,
            <Button key="STOP_BUTTON" onMouseMove={onPreview}>Остановить</Button>
          ] : null}

          {state === 'PREVIEW' && blob ? [
            <Waveforms key="WAVEFORMS" blob={blob}/>,
            <Button key="DISSOLVE_BUTTON" onMouseMove={onDissolve}>Растворить</Button>
          ] : null}
        </CSSTransitionGroup>

        <CSSTransitionGroup
          component="div"
          transitionEnterTimeout={4000}
          transitionLeaveTimeout={1000}
          transitionName={{
            enter: styles.enterball,
            enterActive: styles.enterballActive,
            leave: styles.leaveball,
            leaveActive: styles.leaveballActive
          }}
          >
          {state === 'SAVE_OR_DELETE' && url ? (
            <div key="BALL" className={cx(styles.ball, {[styles.white]: this.props.ball})}/>
          ) : null}
        </CSSTransitionGroup>

        <CSSTransitionGroup
          component="div"
          transitionEnterTimeout={5000}
          transitionLeaveTimeout={1000}
          transitionName={{
            enter: styles.enterfooter,
            enterActive: styles.enterfooterActive,
            leave: styles.leavefooter,
            leaveActive: styles.leavefooterActive
          }}
          >
          {state === 'SAVE_OR_DELETE' && url ? (
            <SaveOrRemove onRemove={onRemove} onSave={onSave} url={url}/>
          ) : null}
        </CSSTransitionGroup>

        <Particles count={this.props.removedStars} opacity={0.8} id="removed-particles" color="#ffffff"/>
        <Particles count={this.props.savedStars} opacity={0.8} id="saved-particles" color="#cb6e2b"/>
      </div>
    );
  }
}

export const mapStateToProps = state => ({
  state: $$recorder.select.state(state),
  blob: $$recorder.select.blob(state),
  url: $$recorder.select.url(state),
  removedStars: $$stars.select.removed(state),
  savedStars: $$stars.select.saved(state),
  ball: $$ball.select(state)
});

export const mapDispatchToProps = {
  onStart: $$recorder.actions.start,
  onPreview: $$recorder.actions.preview,
  onDissolve: $$recorder.actions.dissolve,
  onSave: $$recorder.actions.chooseSave,
  onRemove: $$recorder.actions.chooseRemove
};

export default connect(mapStateToProps, mapDispatchToProps)(Application);
