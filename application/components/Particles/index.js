import React, {PropTypes as T, Component} from 'react';
import './particles.js';
import styles from './index.sss';

export const params = {
  particles: {
    number: {
      value: 20
    },
    line_linked: {
      enable: false
    },
    size: {
      value: 20,
      random: true
    },
    opacity: {
      value: 0.5
    },
    shape: {
      stroke: {
        width: 3,
        color: '#fff'
      }
    },
    move: {
      speed: 1,
      direction: 'bottom'
    }
  },
  interactivity: {
    events: {
      onhover: {
        enable: false
      },
      onclick: {
        enable: true,
        mode: 'push'
      }
    },
    modes: {
      push: {
        particles_nb: 1
      }
    }
  }
};

export class Particles extends Component {
  static propTypes = {
    id: T.string.isRequired,
    color: T.string.isRequired,
    opacity: T.number.isRequired,
    count: T.number.isRequired
  };

  static defaultProps = {
    color: '#ffffff',
    opacity: 0.5,
    count: 20
  };

  $canvas = null;

  state = {
    width: 0,
    height: 0
  };

  render() {
    return (
      <div
        id={this.props.id}
        className={styles.root}
        width={this.state.width}
        height={this.state.height}
        ref={this.getCanvasDOMNode}
      />
    );
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize, false);
    this.handleResize();
    particlesJS(this.props.id, {
      ...params,
      particles: {
        ...params.particles,
        number: {value: this.props.count},
        color: {value: this.props.color},
        opacity: {value: this.props.opacity},
        shape: {stroke: {...params.particles.shape.stroke, color: this.props.color}}
      }
    });
  }

  componentWillReceiveProps(prevProps) {
    if (this.props.count !== prevProps.count) {
      window.pJSDom.map(({pJS}) => {
        if (pJS.canvas.el.parentNode === this.$canvas) {
          pJS.fn.modes.pushParticles(1, {pos_x: window.innerWidth / 2, pos_y: window.innerHeight / 2}, (20 * (Math.random() + .5)));
        }
      });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize, false);
  }

  getCanvasDOMNode = $node => {
    if ($node && !this.$canvas) {
      this.$canvas = $node;
    }
  }

  handleResize = () => this.setState({width: window.innerWidth, height: window.innerHeight});
}

export default Particles;
