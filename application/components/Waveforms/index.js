import React, {PropTypes as T, Component} from 'react';
import moment from 'moment';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import WaveSurfer from 'wavesurfer.js';
import styles from './index.sss';

class Waveforms extends Component {
  static propTypes = {
    blob: T.object.isRequired
  };

  state = {
    duration: null,
    time: null
  };

  render() {
    const {duration, time} = this.state;

    return (
      <div>
        <div id="WAVEFORMS" className={styles.root}/>

        <CSSTransitionGroup
          component="div"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={500}
          transitionName={{
            enter: styles.enter,
            enterActive: styles.enterActive,
            leave: styles.leave,
            leaveActive: styles.leaveActive
          }}
          >
          {duration && time ? (
            <div className={styles.container}>
              <svg className={styles.icon} viewBox="0 0 100 125">
                <path d="M66.853 69.518a3.452 3.452 0 1 1-2.68-6.365 14.77 14.77 0 0 0 4.607-3.125 14.61 14.61 0 0 0 1.785-2.162c.538-.797.988-1.617 1.339-2.446.374-.886.663-1.808.854-2.761.181-.893.275-1.86.275-2.9s-.096-2.01-.275-2.901a14.332 14.332 0 0 0-.854-2.76 14.431 14.431 0 0 0-1.339-2.445 14.748 14.748 0 0 0-1.785-2.162 14.602 14.602 0 0 0-4.607-3.125A3.454 3.454 0 1 1 66.853 30a21.54 21.54 0 0 1 6.83 4.588 21.507 21.507 0 0 1 2.625 3.19 21.435 21.435 0 0 1 1.962 3.638 21.323 21.323 0 0 1 1.262 4.088c.286 1.42.438 2.845.438 4.256s-.15 2.836-.438 4.256a21.248 21.248 0 0 1-1.262 4.087 21.493 21.493 0 0 1-1.962 3.64 21.404 21.404 0 0 1-5.815 5.817 21.953 21.953 0 0 1-3.64 1.958z"/>
                <path d="M73.136 81.207a3.454 3.454 0 1 1-2.68-6.366 27.166 27.166 0 0 0 4.59-2.501 27.36 27.36 0 0 0 4.051-3.344 27.451 27.451 0 0 0 5.844-8.639 27.173 27.173 0 0 0 1.602-5.175c.345-1.708.525-3.521.525-5.426s-.182-3.719-.525-5.427a26.892 26.892 0 0 0-1.602-5.173 27.451 27.451 0 0 0-5.844-8.64 27.415 27.415 0 0 0-4.051-3.343 27.237 27.237 0 0 0-4.59-2.501 3.454 3.454 0 0 1 2.68-6.366 34.078 34.078 0 0 1 5.784 3.124c1.836 1.238 3.535 2.639 5.078 4.182s2.944 3.244 4.183 5.079a34.187 34.187 0 0 1 5.131 12.284c.452 2.239.688 4.506.688 6.782 0 2.274-.236 4.544-.688 6.78a34.178 34.178 0 0 1-5.131 12.283 34.246 34.246 0 0 1-4.183 5.08 34.095 34.095 0 0 1-10.862 7.307zM46.121 17.202c-6.903 5.197-13.808 10.396-20.712 15.592H11.966C8.686 32.794 6 35.479 6 38.759v22.483c0 3.281 2.686 5.965 5.966 5.965h13.442L46.12 82.799c3.147 2.367 6.793 1.988 6.793-2.121V19.324c.001-4.11-3.644-4.491-6.792-2.122z"/>
              </svg>
              <time className={styles.time}>{this.state.time}</time>
              <time className={styles.duration}>{this.state.duration}</time>
            </div>
          ) : null}
        </CSSTransitionGroup>
      </div>
    );
  }

  componentDidMount() {
    this.mounted = true;
    const wavesurfer = new WaveSurfer.create({
      container: '#WAVEFORMS',
      interact: false,
      height: 300,
      cursorColor: '#cb6e2b',
      progressColor: '#cb6e2b'
    });
    this.wavesurfer = wavesurfer;
    wavesurfer.loadBlob(this.props.blob);
    wavesurfer.on('ready', () => {
      setTimeout(() => {
        if (this.mounted) {
          wavesurfer.play();
        }
      }, 2000);
    });

    wavesurfer.on('audioprocess', length => {
      if (this.mounted) {
        this.setState({
          duration: this.convert(wavesurfer.getDuration()),
          time: this.convert(wavesurfer.getCurrentTime())
        });
      }
    });

    wavesurfer.on('finish', () => {
      if (this.mounted) {
        wavesurfer.play();
      }
    });
  }

  componentWillUnmount() {
    this.mounted = false;

    if (this.wavesurfer) {
      this.wavesurfer.destroy();
    }
  }

  convert(s) {
    const minutes = Math.floor(s / 60);
    const seconds = s - minutes * 60;

    return moment(`${minutes}:${seconds}`, 'm:s').format('mm:ss');
  }
}

export default Waveforms;
