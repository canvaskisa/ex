import React, {PropTypes as T} from 'react';
import styles from './index.sss';

const SaveOrRemove = ({url, onSave, onRemove}) => (
  <div className={styles.root}>
    <button className={styles.button} type="button" onMouseMove={onRemove}>Удалить</button>
    <button className={styles.button} type="button" omMouseMove={onSave}>Сохранить</button>
  </div>
);

SaveOrRemove.propTypes = {
  onSave: T.func.isRequired,
  onRemove: T.func.isRequired,
  url: T.string.isRequired
};

export default SaveOrRemove;
