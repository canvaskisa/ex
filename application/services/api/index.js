import axios from 'axios';
import moment from 'moment';

const instance = axios.create({headers: {Accept: 'application/json'}});

export const save = () =>
  instance.post('/save');

export const remove = () =>
  instance.post('/remove');

export const file = blob => {
  const data = new FormData();
  data.append('blob', blob);
  return instance.post('/file', data, {headers: {'Content-Type': 'multipart/form-data'}});
}
