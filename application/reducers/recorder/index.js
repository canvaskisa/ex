// 🗿 Constants
export const constants = {
  SET_INITIAL_STATE: 'SET_INITIAL_STATE',
  SET_RECORDING_STARTED_STATE: 'SET_RECORDING_STARTED_STATE',
  SET_PREVIEW_STATE: 'SET_PREVIEW_STATE',
  SET_SAVE_OR_DELETE_STATE: 'SET_SAVE_OR_DELETE_STATE',
  SET_PREFINISH: 'SET_PREFINISH',

  SET_DOWNLOAD_URL: 'SET_DOWNLOAD_URL',
  CLEAR_DOWNLOAD_URL: 'CLEAR_DOWNLOAD_URL',
  SET_BLOB: 'SET_BLOB',
  CLEAR_BLOB: 'CLEAR_BLOB',

  CHOOSE_SAVE: 'CHOOSE_SAVE',
  CHOOSE_REMOVE: 'CHOOSE_REMOVE'
};

// 📽 Actions
export const actions = {
  setDownloadUrl: url => ({type: constants.SET_DOWNLOAD_URL, url}),
  clearDownloadUrl: () => ({type: constants.CLEAR_DOWNLOAD_URL}),
  finish: () => ({type: constants.SET_INITIAL_STATE}),
  prefinish: () => ({type: constants.SET_PREFINISH}),
  start: () => ({type: constants.SET_RECORDING_STARTED_STATE}),
  preview: () => ({type: constants.SET_PREVIEW_STATE}),
  dissolve: () => ({type: constants.SET_SAVE_OR_DELETE_STATE}),
  setBlob: blob => ({type: constants.SET_BLOB, blob}),
  clearBlob: () => ({type: constants.CLEAR_BLOB}),

  chooseSave: () => ({type: constants.CHOOSE_SAVE}),
  chooseRemove: () => ({type: constants.CHOOSE_REMOVE})
};

// 👇 Selectors
export const select = {
  state: state => state.recorder.state,
  url: state => state.recorder.url,
  blob: state => state.recorder.blob
};

// 🐣 Initial state
export const initialState = {
  state: 'INITIAL', // INITIAL || RECORDING_STARTED || PREVIEW || PREFINISH || SAVE_OR_DELETE
  url: null,
  blob: null
};

// 🔪 Reducer
export default (state = initialState, action) => {
  switch (action.type) {
    case constants.SET_INITIAL_STATE:
      return initialState;

    case constants.SET_RECORDING_STARTED_STATE:
      return ({...state, state: 'RECORDING_STARTED'});

    case constants.SET_PREVIEW_STATE:
      return ({...state, state: 'PREVIEW'});

    case constants.SET_SAVE_OR_DELETE_STATE:
      return ({...state, state: 'SAVE_OR_DELETE'});

    case constants.SET_PREFINISH:
      return ({...state, state: 'PREFINISH'});

    case constants.SET_DOWNLOAD_URL:
      return ({...state, url: action.url});

    case constants.CLEAR_DOWNLOAD_URL:
      return ({...state, url: null});

    case constants.SET_BLOB:
      return ({...state, blob: action.blob});

    case constants.CLEAR_BLOB:
      return ({...state, blob: null});

    default:
      return state;
  }
};
