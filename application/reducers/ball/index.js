import * as $$recorder from 'reducers/recorder';

// 🗿 Constants
export const constants = {
  WHITIFY_BALL: 'WHITIFY_BALL'
};

// 📽 Actions
export const actions = {
  whitify: () => ({type: constants.WHITIFY_BALL})
};

// 👇 Selectors
export const select = state => state.ball;

// 🐣 Initial state
export const initialState = false;

// 🔪 Reducer
export default (state = initialState, action) => {
  switch (action.type) {
    case $$recorder.constants.SET_INITIAL_STATE:
      return false;

    case constants.WHITIFY_BALL:
      return true;

    default:
      return state;
  }
};
