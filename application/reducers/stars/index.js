// 🗿 Constants
export const constants = {
  ADD_REMOVED: 'ADD_REMOVED',
  ADD_SAVED: 'ADD_SAVED'
};

// 📽 Actions
export const actions = {
  remove: () => ({type: constants.ADD_REMOVED}),
  save: () => ({type: constants.ADD_SAVED})
};

// 👇 Selectors
export const select = {
  saved: state => state.stars.saved,
  removed: state => state.stars.removed
};

// 🐣 Initial state
export const initialState = {
  saved: window.SAVED_STARS || 0,
  removed: window.REMOVED_STARS || 0
};

// 🔪 Reducer
export default (state = initialState, action) => {
  switch (action.type) {
    case constants.ADD_REMOVED:
      return ({...state, removed: state.removed + 1});

    case constants.ADD_SAVED:
      return ({...state, saved: state.saved + 1});

    default:
      return state;
  }
};
